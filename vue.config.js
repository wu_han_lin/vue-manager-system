module.exports = {
    //publicPath: '././', //项目打包时使用，否则访问出现404
    outputDir: 'dist', //build构建文件的目录
    assetsDir:'static', //放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录
    runtimeCompiler: false, //是否使用包含运行时编译器的 Vue 构建版本
    productionSourceMap: false, //是否为生产环境构建生成 source map？这样打包的dist文件体积会很小（正式版）
    devServer: {
      proxy: {
        '/api': { //代理以 api 开头的请求
          target: 'http://127.0.0.1:80/', //当前请求地址代理到配置的目标地址上
          ws: true,              // 是否启用websockets
          changeOrigin: true,    //开启代理
          secure: false,       // 将安全设置为false,才能访问https开头的
          pathRewrite: {
              '^/api': '' //api 开头的地址 替换为 '' 空开头，即去除 api 
          }
        }
      }
    }
}