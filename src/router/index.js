import Vue from 'vue';
import Router from 'vue-router';

import Home from '@/components/Home'

import BaseTable from '@/views/base/BaseTable'
import Permission from '@/views/base/Permission'
import Tabs from '@/views/base/Tabs'
import BaseForm from '@/views/base/BaseForm'
import UserInfo from '@/views/base/UserInfo'
import Icon from '@/views/base/Icon'
import BaseCharts from '@/views/base/BaseCharts'

import Login from '@/views/Login'
import E404 from '@/views/404'
import E403 from '@/views/403'
import Welcome from '@/views/Welcome'

// 营销机会
import Opportunity from '@/views/market/Opportunity'
import OpportunityDetail from '@/views/market/OpportunityDetail'

// 开发计划
import DevelopmentPlan from '@/views/market/DevelopmentPlan'
import DevelopmentPlanDetail from '@/views/market/DevelopmentPlanDetail'

Vue.use(Router);

const routes = [
    {
        name: '默认页面',
        path: '/',
        redirect: '/login'
    },
    {
        name: '首页',
        path: '/',
        component: Home,
        children: [
            {
                path: '/opportunity',
                component: Opportunity
            },
            {
                path: '/opportunity/:type/:id',
                component: OpportunityDetail
            },
            {
                path: '/developmentPlan',
                component: DevelopmentPlan
            },
            {
                path: '/developmentPlan/:type',
                component: DevelopmentPlanDetail
            },
            {
                path: '/welcome',
                component: Welcome
            },
            {
                path: '/userInfo',
                component: UserInfo
            },
            {
                path: '/icon',
                component: Icon
            },
            {
                path: '/table',
                component: BaseTable
            },
            {
                path: '/chcarts',
                component: BaseCharts
            },
            {
                path: '/tabs',
                component: Tabs
            },
            {
                path: '/form',
                component: BaseForm
            },
            {
                name: 'permission',
                path: '/permission',
                component: Permission,
                meta: { //权限认证标识
                    permission: true
                }
            },
            {
                path: '/404',
                component: E404
            },
            {
                path: '/403',
                component: E403
            }
        ]
    },
    {
        path: '/login',
        component: Login
    },
    {
        path: '*',
        redirect: '/404'
    }
]

let router = new Router({
    mode: 'history',
    //base: '/app/', //默认值: "/",项目打包部署的时候使用，取了一个项目名称
    routes
});

//使用钩子函数对路由进行权限跳转
// router.beforeEach((to, from, next) => {
//     document.title = `客户关系管理系统`;
//     if (to.path !== '/login') {
//         next('/login');
//     } else {
//         // 简单的判断IE10及以下不进入富文本编辑器，该组件不兼容
//         if (navigator.userAgent.indexOf('MSIE') > -1 && to.path === '/editor') {
//             Vue.prototype.$alert('vue-quill-editor组件不兼容IE10及以下浏览器，请使用更高版本的浏览器查看', '浏览器不兼容通知', {
//                 confirmButtonText: '确定'
//             });
//         } else {
//             next();
//         }
//     }
// });

export default router;