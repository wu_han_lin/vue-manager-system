import Vue from 'vue';
import App from './App.vue';
import router from './router';
import ElementUI from 'element-ui';

import 'element-ui/lib/theme-chalk/index.css'; // 默认主题
import './assets/directives'; //弹出层拖拽组件
import 'babel-polyfill';

//axios 工具类封装
// import Http from '@/util/http';
// Vue.use(Http);

//后端请求统一绑定
import api from '@/api';
Vue.prototype.$api = api;

Vue.config.productionTip = false;
Vue.use(ElementUI);

//vuex 状态管理
import store from './sotre'

//通过EventBus进行兄弟间组件通讯
export const eventBus = new Vue();

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
