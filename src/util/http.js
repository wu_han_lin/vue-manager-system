import axios from 'axios'
import router from '@/router'

// qs 是对 post 请求 data 进行处理，以 formData的形式提交
import qs from 'qs'
import { Message, Loading } from 'element-ui'

axios.defaults.timeout = 5000 // 5s没响应则认为该请求失败
//axios.defaults.withCredentials = true	// 跨域是使用凭证，如果要带上cookie话则需要设置withCrendentials
// 配置请求头
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8' 
//axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8' 
//配置baseUrl，所有的 axios 请求地址都会加上 /api
axios.defaults.baseURL = '/api'  //跨域配置可以添加上它，就不用每一个请求都以api开头了

let loadingInstance
// POST传参序列化(添加请求拦截器
axios.interceptors.request.use(request => {
    //TODO 开始请求，日志打印
    console.log("开始请求:",request.url,"，参数：",JSON.stringify(request.params))

    loadingInstance = Loading.service({
      lock: true,
      text: '数据加载中，请稍后...',
      spinner: 'el-icon-loading',
      background: 'rgba(0, 0, 0, 0.7)'
    })

    // 获取token
    //const token = localStorage.getItem('cc_token');
    // 添加token到headers
    // if(token){
    //   config.headers.token = token
    // }

    if (request.method === 'post') {
      request.data = qs.stringify(request.data)  //这里使用qs对data就行处理
    }

    return request
  },
  err => {
    loadingInstance.close()
    Message.error('请求超时!')
    return Promise.reject(err)
  }
)
// 返回状态判断(添加响应拦截器)
axios.interceptors.response.use(response => {
    //TODO 请求完毕数据输出,后续添加后端返回的对应状态判断
    console.log("请求完毕数据：",response.data);

    loadingInstance.close()
  
    if (response.data.code === 200) {
      return response.data
    }else if (response.data.code === 504) {
      Message.error('服务器被吃了⊙﹏⊙∥');
    } else if(response.data.code === 404){
      Message.error('请求地址不存在!');
      router.replace({path:'/404'})
    }else if (response.data.code === 403) {
      Message.error('权限不足,请联系管理员!');
      router.replace({path:'/403'})
    } else if (response.data.code === 401) { //未登录
      // 跳转登录页面，并将要浏览的页面fullPath传过去，登录成功后跳转需要访问的页面 
      router.replace({    
        path: '/login',    
        query: { 
          redirect: router.currentRoute.fullPath 
        }   
      });   
    }else{
      Message.error(response.data.msg);
    }
    return response.data
  },err => {
    loadingInstance.close()
    Message.error('请求失败，请稍后再试');
    return Promise.reject(err)
  }
)
// 请求封装，需要在 mian.js 中 通过 Vue.use(http) 挂载到vue实例上，其它实例中就通过 this.$get 来访问
// install: (Vue) => {
//   Vue.prototype.$get = (url, params = {}) => {
//     return axios.get(url, {params})
//   }
//   Vue.prototype.$post = (url, params = {}) => {
//     return axios.post(url, params)
//   }
//   Vue.prototype.$delete = (url, params = {}) => {
//     return axios.delete(url, {params})
//   }
//   Vue.prototype.$put = (url, params = {}) => {
//     return axios.put(url, {params})
//   }
//   Vue.prototype.$upload = (url, params = {}) => {
//     return axios.post(url, params,{
//       headers: { 
//         'Content-Type': 'multipart/form-data' 
//       }
//     })
//   }
// };
export default {
  get(url, params = {}){
    return axios.get(url, {params})
  },
  post(url, params = {}){
    return axios.post(url, params)
  },
  delete(url, params = {}){
    return axios.delete(url, {params})
  },
  put(url, params = {}){
    return axios.put(url, {params})
  },
  upload(url, params = {}){
    return axios.post(url, params,{
      headers: { 
        'Content-Type': 'multipart/form-data' 
      }
    })
  }
};