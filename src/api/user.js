import http from '@/util/http';

export default {
    login(params){
        return http.post('/login',params);
    },
    logout(){
        return http.get('/logout');
    },
    list(params){
        return http.get('/json/table.json',params);
    }
}